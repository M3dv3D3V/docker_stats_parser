import json
import asyncio
from time import sleep
from datetime import datetime, timedelta


TIMEOUT_UPDATE: int = 3
TIME_OF_STATISTICS_COLLECTION: int = 0


TIME_STATISTIC_BY_CONTAINERS: dict = {
    'container_names...': {}
}


async def run(cmd):
    process = await asyncio.create_subprocess_shell(
        cmd,
        stdout=asyncio.subprocess.PIPE,
        stderr=asyncio.subprocess.PIPE
    )

    stdout, stderr = await process.communicate()
    if stdout:
        return stdout


def parse_data_stdout(data: dict) -> tuple:
    name: str = data.get('Name')
    raw_mem: str = data.get('MemUsage')
    mem_size: str = 'B'
    if 'MiB' in raw_mem:
        mem_size = 'MiB'
    elif 'GiB' in raw_mem:
        mem_size = 'GiB'

    mem = raw_mem.split()[0].strip(mem_size)
    cpu: str = data.get('CPUPerc').strip('%')
    return name, float(cpu), float(mem), mem_size


async def main():
    coros: list = []
    for container_name in TIME_STATISTIC_BY_CONTAINERS.keys():
        cmd = 'docker stats {container_name} --no-stream --format "{format_str}"'.format(
            container_name=container_name,
            format_str="{{ json .}}"
        )
        coros.append(
            asyncio.create_task(run(cmd))
        )

    for coro in asyncio.as_completed(coros):
        try:
            tmp_result = await coro
            if tmp_result:
                data: dict = json.loads(tmp_result.decode('utf-8'))
                current_name, current_cpu, current_mem, mem_size = parse_data_stdout(data)
                if TIME_STATISTIC_BY_CONTAINERS.get(current_name) == {}:
                    TIME_STATISTIC_BY_CONTAINERS[current_name] = {
                        'cpu': current_cpu,
                        'mem': {'value': current_mem, 'mem_size': mem_size},
                        'quantity': 0
                    }
                    continue

                cpu: float = TIME_STATISTIC_BY_CONTAINERS[current_name].get(
                    'cpu'
                )
                mem: float = TIME_STATISTIC_BY_CONTAINERS[current_name].get(
                    'mem'
                ).get('value')
                quantity: int = TIME_STATISTIC_BY_CONTAINERS[current_name].get(
                    'quantity'
                ) + 1
                TIME_STATISTIC_BY_CONTAINERS[current_name] = {
                    'cpu': cpu + current_cpu,
                    'mem': {
                        'value': mem + current_mem,
                        'mem_size': mem_size
                    },
                    'quantity': quantity
                }
        except asyncio.TimeoutError:
            print('error')


if __name__ == '__main__':
    start_time = datetime.now()
    current_time = datetime.now()
    while current_time < start_time + timedelta(minutes=TIME_OF_STATISTICS_COLLECTION):
        print('thinking...')
        current_time = datetime.now()
        time_left = start_time + timedelta(
            minutes=TIME_OF_STATISTICS_COLLECTION) - current_time
        print(f'time left: {time_left}')
        asyncio.run(main())
        sleep(TIMEOUT_UPDATE)

    for container_name, container_statistic in TIME_STATISTIC_BY_CONTAINERS.items():
        quantity = TIME_STATISTIC_BY_CONTAINERS[container_name].get(
            'quantity'
        )
        if not quantity:
            continue

        cpu = TIME_STATISTIC_BY_CONTAINERS[container_name].get(
            'cpu'
        ) / quantity
        mem = TIME_STATISTIC_BY_CONTAINERS[container_name].get('mem').get(
            'value'
        ) / quantity
        mem_size = TIME_STATISTIC_BY_CONTAINERS[container_name].get('mem').get(
            'mem_size'
        )
        print(container_name)
        print(f'cpu: {cpu}%')
        print(f'mem: {mem}{mem_size}')
        print()


